/*!
 * jQuery.ScrollTo
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * Date: 12/14/2012
 *
 * @projectDescription Easy element scrolling using jQuery.
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @author Ariel Flesler
 * @version 1.4.5 BETA
 *
 * @id jQuery.scrollTo
 * @id jQuery.fn.scrollTo
 * @param {String, Number, DOMElement, jQuery, Object} target Where to scroll the matched elements.
 *    The different options for target are:
 *    - A number position (will be applied to all axes).
 *    - A string position ('44', '100px', '+=90', etc ) will be applied to all axes
 *    - A jQuery/DOM element ( logically, child of the element to scroll )
 *    - A string selector, that will be relative to the element to scroll ( 'li:eq(2)', etc )
 *    - A hash { top:x, left:y }, x and y can be any kind of number/string like above.
 *    - A percentage of the container's dimension/s, for example: 50% to go to the middle.
 *    - The string 'max' for go-to-end.
 * @param {Number, Function} duration The OVERALL length of the animation, this argument can be the settings object instead.
 * @param {Object,Function} settings Optional set of settings or the onAfter callback.
 *   @option {String} axis Which axis must be scrolled, use 'x', 'y', 'xy' or 'yx'.
 *   @option {Number, Function} duration The OVERALL length of the animation.
 *   @option {String} easing The easing method for the animation.
 *   @option {Boolean} margin If true, the margin of the target element will be deducted from the final position.
 *   @option {Object, Number} offset Add/deduct from the end position. One number for both axes or { top:x, left:y }.
 *   @option {Object, Number} over Add/deduct the height/width multiplied by 'over', can be { top:x, left:y } when using both axes.
 *   @option {Boolean} queue If true, and both axis are given, the 2nd axis will only be animated after the first one ends.
 *   @option {Function} onAfter Function to be called after the scrolling ends.
 *   @option {Function} onAfterFirst If queuing is activated, this function will be called after the first scrolling ends.
 * @return {jQuery} Returns the same jQuery object, for chaining.
 *
 * @desc Scroll to a fixed position
 * @example $('div').scrollTo( 340 );
 *
 * @desc Scroll relatively to the actual position
 * @example $('div').scrollTo( '+=340px', { axis:'y' } );
 *
 * @desc Scroll using a selector (relative to the scrolled element)
 * @example $('div').scrollTo( 'p.paragraph:eq(2)', 500, { easing:'swing', queue:true, axis:'xy' } );
 *
 * @desc Scroll to a DOM element (same for jQuery object)
 * @example var second_child = document.getElementById('container').firstChild.nextSibling;
 *      $('#container').scrollTo( second_child, { duration:500, axis:'x', onAfter:function(){
 *        alert('scrolled!!');
 *      }});
 *
 * @desc Scroll on both axes, to different values
 * @example $('div').scrollTo( { top: 300, left:'+=200' }, { axis:'xy', offset:-20 } );
 */

;(function( $ ){

  var $scrollTo = $.scrollTo = function( target, duration, settings ){
    $(window).scrollTo( target, duration, settings );
  };

  $scrollTo.defaults = {
    axis:'xy',
    duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1,
    limit:true
  };

  // Returns the element that needs to be animated to scroll the window.
  // Kept for backwards compatibility (specially for localScroll & serialScroll)
  $scrollTo.window = function( scope ){
    return $(window)._scrollable();
  };

  // Hack, hack, hack :)
  // Returns the real elements to scroll (supports window/iframes, documents and regular nodes)
  $.fn._scrollable = function(){
    return this.map(function(){
      var elem = this,
        isWin = !elem.nodeName || $.inArray( elem.nodeName.toLowerCase(), ['iframe','#document','html','body'] ) != -1;

        if( !isWin )
          return elem;

      var doc = (elem.contentWindow || elem).document || elem.ownerDocument || elem;

      return /webkit/i.test(navigator.userAgent) || doc.compatMode == 'BackCompat' ?
        doc.body :
        doc.documentElement;
    });
  };

  $.fn.scrollTo = function( target, duration, settings ){
    if( typeof duration == 'object' ){
      settings = duration;
      duration = 0;
    }
    if( typeof settings == 'function' )
      settings = { onAfter:settings };

    if( target == 'max' )
      target = 9e9;

    settings = $.extend( {}, $scrollTo.defaults, settings );
    // Speed is still recognized for backwards compatibility
    duration = duration || settings.duration;
    // Make sure the settings are given right
    settings.queue = settings.queue && settings.axis.length > 1;

    if( settings.queue )
      // Let's keep the overall duration
      duration /= 2;
    settings.offset = both( settings.offset );
    settings.over = both( settings.over );

    return this._scrollable().each(function(){
      // Null target yields nothing, just like jQuery does
      if (target == null) return;

      var elem = this,
        $elem = $(elem),
        targ = target, toff, attr = {},
        win = $elem.is('html,body');

      switch( typeof targ ){
        // A number will pass the regex
        case 'number':
        case 'string':
          if( /^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ) ){
            targ = both( targ );
            // We are done
            break;
          }
          // Relative selector, no break!
          targ = $(targ,this);
          if (!targ.length) return;
        case 'object':
          // DOMElement / jQuery
          if( targ.is || targ.style )
            // Get the real position of the target
            toff = (targ = $(targ)).offset();
      }
      $.each( settings.axis.split(''), function( i, axis ){
        var Pos = axis == 'x' ? 'Left' : 'Top',
          pos = Pos.toLowerCase(),
          key = 'scroll' + Pos,
          old = elem[key],
          max = $scrollTo.max(elem, axis);

        if( toff ){// jQuery / DOMElement
          attr[key] = toff[pos] + ( win ? 0 : old - $elem.offset()[pos] );

          // If it's a dom element, reduce the margin
          if( settings.margin ){
            attr[key] -= parseInt(targ.css('margin'+Pos)) || 0;
            attr[key] -= parseInt(targ.css('border'+Pos+'Width')) || 0;
          }

          attr[key] += settings.offset[pos] || 0;

          if( settings.over[pos] )
            // Scroll to a fraction of its width/height
            attr[key] += targ[axis=='x'?'width':'height']() * settings.over[pos];
        }else{
          var val = targ[pos];
          // Handle percentage values
          attr[key] = val.slice && val.slice(-1) == '%' ?
            parseFloat(val) / 100 * max
            : val;
        }

        // Number or 'number'
        if( settings.limit && /^\d+$/.test(attr[key]) )
          // Check the limits
          attr[key] = attr[key] <= 0 ? 0 : Math.min( attr[key], max );

        // Queueing axes
        if( !i && settings.queue ){
          // Don't waste time animating, if there's no need.
          if( old != attr[key] )
            // Intermediate animation
            animate( settings.onAfterFirst );
          // Don't animate this axis again in the next iteration.
          delete attr[key];
        }
      });

      animate( settings.onAfter );

      function animate( callback ){
        $elem.animate( attr, duration, settings.easing, callback && function(){
          callback.call(this, target, settings);
        });
      };

    }).end();
  };

  // Max scrolling position, works on quirks mode
  // It only fails (not too badly) on IE, quirks mode.
  $scrollTo.max = function( elem, axis ){
    var Dim = axis == 'x' ? 'Width' : 'Height',
      scroll = 'scroll'+Dim;

    if( !$(elem).is('html,body') )
      return elem[scroll] - $(elem)[Dim.toLowerCase()]();

    var size = 'client' + Dim,
      html = elem.ownerDocument.documentElement,
      body = elem.ownerDocument.body;

    return Math.max( html[scroll], body[scroll] )
       - Math.min( html[size]  , body[size]   );
  };

  function both( val ){
    return typeof val == 'object' ? val : { top:val, left:val };
  };

})( jQuery );


// Generated by CoffeeScript 1.6.1
// usage: $('.container').snapscroll({'scrollOptions':{'axis':'y'}});
// for scrollOptions refer to https://github.com/flesler/jquery.scrollTo
(function($, window, document, undefined_) {
  var Plugin, defaults, pluginName;
  pluginName = "snapscroll";
  defaults = {
    botPadding: 40,
    topPadding: 40,
    scrollSpeed: 300,
    scrollEndSpeed: 100,
    scrollOptions: {'axis':'xy'}
  };
  Plugin = function(element, options) {
    this.container = $(element);
    this.options = $.extend({}, defaults, options);
    return this.init();
  };
  Plugin.prototype = {
    init: function() {
      return this.snapping();
    },
    snapping: function() {
      var $children, autoscrolling, prev_position, scroll_end_speed, scroll_speed, timer,
        _this = this;
      $children = this.container.children();
      scroll_speed = this.options.scrollSpeed;
      scroll_end_speed = this.options.scrollEndSpeed;
    scroll_options = this.options.scrollOptions;
      prev_position = $(document).scrollTop();
      timer = null;
      autoscrolling = false;
      return $(window).off("scroll.snapscroll").on("scroll.snapscroll", function() {
        var $child, cur_position, direction;
        if (!autoscrolling) {
          cur_position = $(document).scrollTop();
          direction = _this.getDirection(prev_position, cur_position);
          $child = _this.getTargetChild($children, direction, cur_position);
          if ($child) {
            clearTimeout(timer);
            timer = setTimeout(function() {
              $(window).scrollTo($child, scroll_speed, scroll_options);
              $child.siblings(".ss-active").removeClass("ss-active");
              $child.addClass("ss-active");
              autoscrolling = true;
              return setTimeout(function() {
                prev_position = $(document).scrollTop();
                return autoscrolling = false;
              }, scroll_speed + 20);
            }, scroll_end_speed);
          }
          return prev_position = cur_position;
        }
      });
    },
    getDirection: function(a, b) {
      if (a > b) {
        return "up";
      } else {
        return "down";
      }
    },
    getTargetChild: function($children, direction, position) {
      var $target, bottom_position, fc_top, lc_bottom, options, window_height;
      options = this.options;
      window_height = $(window).height();
      bottom_position = position + window_height;
      fc_top = $children.first().offset().top;
      lc_bottom = $children.last().offset().top + window_height;
      $target = null;
      if (fc_top < position + options.topPadding) {
        $children.not(".ss-active").each(function(i) {
          var object_bot, object_top;
          object_top = $(this).offset().top;
          object_bot = object_top + $(this).height();
          if (direction === "down") {
            if (object_top < bottom_position && object_bot > position) {
              $target = $(this);
              return false;
            }
          } else {
            if (object_top < position && position < object_bot) {
              return $target = $(this);
            }
          }
        });
      }
      return $target;
    }
  };
  return $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, "plugin_" + pluginName)) {
        return $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document);


$(function() {
  $("body").snapscroll();
});
