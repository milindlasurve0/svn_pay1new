/* 	Author:Prashant Sani
	Yellow Slice
	www.yellowslice.in
*/
//Call back for PNGs for non-supporting SVG devices.
if (!Modernizr.svg) {
  $('img[src*="svg"]').attr('src', function() {
      return $(this).attr('src').replace('.svg', '.png');
  });            
} 

/* Back to Top */
$('.backToTop').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 800);
	return false;
});

$('.toggleMobileMenu').click(function(){
  $(this).toggleClass('toggleMobileMenuOpen');
    $('.mainNav').slideToggle();
});

function mobileMenu(){
  if(winWidth<700){
    if(!$('body').hasClass('mobile')){
      $('body').addClass('mobile');
      $('.mainNav').insertBefore($('header'))
    }
    else{
      return;
    }
  }
  else{
    if($('body').hasClass('mobile')){
      $('body').removeClass('mobile');
      $('.mainNav').insertBefore($('.socialIcons'))
    }
    else{
      return;
    }
  }
}

//Header Shrink
function HeaderShirnk(){
  var scroll = $(window).scrollTop();
  if(winWidth>700){
    if (scroll <= 90) {
      $('body').removeClass('scrolledMoreThan90').addClass('scrolledLessThan90')
    }
    else{
      $('body').removeClass('scrolledLessThan90').addClass('scrolledMoreThan90') 
    }
  }
}

$(window).scroll(function() {
  HeaderShirnk();
});

$(window).load(function() {
  HeaderShirnk()
  mobileMenu();
});

$(window).resize(function() {
  winHeight = document.body.clientHeight;
  winWidth  = document.body.clientWidth;
  mobileMenu();
});

//Push Footer inside Last Sec
$('.homeAppsSec').append($('.homeFooter'))
$('.aboutHowToUse').append($('.aboutFooter'))

// Partner Form Validation
function validatePartnerForm(){
       
	if(document.getElementById("name").value.trim() == "")
	{
		document.getElementById("errorName").innerHTML="Please enter name";
		document.getElementById("name").focus();
		return false;
	}
	if(document.getElementById("contact").value.trim() == "")
	{
		document.getElementById("errorName").innerHTML="";
		document.getElementById("errorContact").innerHTML="Please enter contact";
		document.getElementById("contact").focus();
		return false;
	}
        
        var phoneValidation = /^\d{10}$/;  
        if(! document.getElementById("contact").value.trim().match(phoneValidation))  
        {  
             document.getElementById("errorName").innerHTML="";
             document.getElementById("errorContact").innerHTML="Please enter contact";
             document.getElementById("contact").focus();
             return false;
        }  
          

        
	if(document.getElementById("email").value.trim() == ""){
		document.getElementById("errorContact").innerHTML="";
		document.getElementById("errorEmail").innerHTML="Please enter email.";
		document.getElementById("email").focus();
		return false;
	}
	if(document.getElementById("email").value.trim() != ''){
		document.getElementById("errorEmail").innerHTML="";	
		var email = document.getElementById("email").value;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email)) {
			//alert("Invalid email address.");
			document.getElementById("errorEmail").innerHTML="Invalid email adrress.";
			document.getElementById("email").focus();
			return false;
		}
	}
	if(document.getElementById("state").value.trim() =="")
	{
		document.getElementById("errorEmail").innerHTML="";
		document.getElementById("errorState").innerHTML="Please select your state.";
		document.getElementById("state").focus();
		return false;
	}
        if(document.getElementById("city").value.trim() == "")
	{
		document.getElementById("errorState").innerHTML="";	
		document.getElementById("errorCity").innerHTML="Please select your city.";
		document.getElementById("city").focus();
		return false;
	}

        if(document.partnerForm.interest[0].checked=="" && document.partnerForm.interest[1].checked=="")
	{
		document.getElementById("errorCity").innerHTML="";	
		document.getElementById("errorInterest").innerHTML="Please Select your interest";
		return false;		
	}
	if(document.getElementById("comment").value.trim()  == "")
	{
		document.getElementById("errorInterest").innerHTML="";	
		document.getElementById("errorComment").innerHTML="Please enter comment";
		document.getElementById("comment").focus();
		return false;
	}
	else{
		document.getElementById("errorComment").innerHTML="";
		return true;
	}
}

// Contact Form
function validateContactForm() {		
	if(  document.getElementById("name").value.trim()=="")
	{	
		
		document.getElementById("errorName").innerHTML="&#9650;&nbsp;Please enter full name.";
		document.getElementById("name").focus();
		return false;		
	}
	if(document.getElementById("contact").value.trim()=="")
	{	
		document.getElementById("errorContact").innerHTML="&#9650;&nbsp;Please enter contact no.";
		document.getElementById("contact").focus();
		document.getElementById("errorName").style.visibility="hidden";
		return false;			
	}
	if(document.getElementById("email").value.trim()==""){
		document.getElementById("errorContact").innerHTML="";
		document.getElementById("errorEmail").innerHTML="Please enter email.";
		document.getElementById("email").focus();
		return false;
	}
	if(document.getElementById("email").value.trim() != ''){
		document.getElementById("errorEmail").innerHTML="";	
		var email = document.getElementById("email").value;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email)) {
			//alert("Invalid email address.");
			document.getElementById("errorEmail").innerHTML="Invalid email adrress.";
			document.getElementById("email").focus();
			return false;
		}
	}
	if(document.getElementById("comment").value.trim()=="")
	{	
		document.getElementById("errorComment").innerHTML="&#9650;&nbsp;Please enter comment.";
		document.getElementById("comment").focus();
		document.getElementById("errorEmail").style.visibility="hidden";
		return false;		
	}
}

// Subscribe Form
function validateSubscribeForm() {		
	if(document.getElementById("name").value=="")
	{		
		document.getElementById("errorName").innerHTML="&#9650;&nbsp;Please enter full name.";
		document.getElementById("name").focus();
		return false;		
	}
	if(document.getElementById("contact").value=="")
	{	
		document.getElementById("errorContact").innerHTML="&#9650;&nbsp;Please enter contact no.";
		document.getElementById("contact").focus();
		document.getElementById("errorName").style.visibility="hidden";
		return false;			
	}
	if(document.getElementById("email").value==""){
		document.getElementById("errorContact").innerHTML="";
		document.getElementById("errorEmail").innerHTML="Please enter email.";
		document.getElementById("email").focus();
		return false;
	}
	if(document.getElementById("email").value != ''){
		document.getElementById("errorEmail").innerHTML="";	
		var email = document.getElementById("email").value;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email)) {
			//alert("Invalid email address.");
			document.getElementById("errorEmail").innerHTML="Invalid email adrress.";
			document.getElementById("email").focus();
			return false;
		}
	}
	else{
		return true;
	}
}